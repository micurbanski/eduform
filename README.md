# EduForm #

**Content Management System** for an e-learning platfrom. It allows instructors to create courses and manage their contents. Students can browse posted courses and enroll for the course of their choice

### Features ###

* Courses, Modules and Contents are managed by Instructors
* Subjects are managed by Admin
* Content can be Text, File, Image or Video
* Student Registration System
* Modules and Contents are sorted automatically
* Drag'n'drop reordering (AJAX)
* Caching (Memcache)
* RESTful API that handles authentication (Django REST Framework

### Requirements ###

* Python 3.3+
* requirements.txt
* [Memcached 1.4.25](http://memcached.org/)