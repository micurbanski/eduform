from django.contrib.auth.models import User

import json
from django.core.urlresolvers import reverse
from django.test import TestCase
from rest_framework.authtoken.models import Token
from rest_framework.test import APIRequestFactory, APIClient

from .models import Subject, Course
# Create your tests here.


class CoursesApiTest(TestCase):

    def setUp(self):
        self.factory = APIRequestFactory()
        self.client = APIClient()
        self.user = User.objects.create_user(username='TestUser',
                                             password='password1',
                                             email='Test@User.com')
        self.user.save()
        token = Token.objects.create(user=self.user)
        token.save()
        self.admin_user = User.objects.create_superuser(
                username='AdminTestUser',
                password='password2',
                email='TestAdmin@User.com')
        self.subject = Subject.objects.create(title='TestSubject',
                                              slug='testslug',
                                              id=99)
        self.course = Course.objects.create(title='TestCourse',
                                            owner=self.admin_user,
                                            slug='testslug',
                                            subject=self.subject,
                                            id=99)

    def test_subject_list(self):
        url = reverse('api:subject_list')
        response = self.client.get(url)
        self.assertContains(response, 'TestSubject')

    def test_subject_detail(self):
        url = reverse('api:subject_detail', args=[99])
        response = self.client.get(url)
        data = json.loads(response.content.decode('utf-8'))
        content = {"id": 99, "title": "TestSubject", "slug": "testslug"}
        self.assertEqual(data, content)

    def test_user_permitted_to_enroll(self):
        self.client.force_authenticate(user=self.user)
        url = reverse('api:course-enroll', args=[self.course.id])
        response = self.client.post(url)
        data = json.loads(response.content.decode('utf-8'))
        content = {'Enrolled': True}
        self.assertEqual(data, content)

    def test_enrolled_permitted_to_view(self):
        self.client.force_authenticate(user=self.user)
        self.course.students.add(self.user)
        url = reverse('api:course-contents', args=[self.course.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_anonuser_not_permitted_to_enroll(self):
        url = reverse('api:course-enroll', args=[self.course.id])
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)

    def test_anonuser_not_permitted_to_view(self):
        url = reverse('api:course-contents', args=[self.course.id])
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)

    def test_notenrolled_not_permitted_to_view(self):
        self.client.force_authenticate(user=self.user)
        url = reverse('api:course-contents', args=[self.course.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)
